<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
    <xsl:output method="html"/>
    <xsl:template match="/root">
    	<div>
    		<xsl:attribute name="class">
                <xsl:for-each select="//classes_array/item">
                	<xsl:value-of select="."/>
                	<xsl:text> </xsl:text>
                </xsl:for-each>               
    		</xsl:attribute>
            <div class="attribution">

                <xsl:value-of select="//picture" disable-output-escaping="yes"/>
            
                <div class="submitted">
                  <p class="commenter-name">
                    <xsl:value-of select="//author" disable-output-escaping="yes"/>
                  </p>
                  <p class="comment-time">
                  	<xsl:value-of select="php:function('format_date', string(//created))" disable-output-escaping="yes"/>
                  </p>
                  <p class="comment-permalink">
                    <xsl:value-of select="//permalink" disable-output-escaping="yes"/>
                  </p>
                </div>
              </div><!-- .attribution -->
            <div class='comment-text'>
            	<div class='comment-arrow'></div>
                <xsl:if test="string-length(./new) &gt; 0">
                	<span class='new'>
                    	<xsl:value-of select="./new" disable-output-escaping="yes"/>
                    </span>	
                </xsl:if>
                <h3 class='title'>
                    <xsl:value-of select='./comment/subject'/>
                    <xsl:if test="./comment/new &gt; 0">
                        <span class='new'><xsl:value-of select="./new"/></span>
                    </xsl:if>
                </h3>
                <div class='content'>
                    <xsl:if test="//unpublished = 1">
                        <div class='unpublished'>
                            <xsl:value-of select="php:function('t', 'Unpublished')"/>
                        </div>
                    </xsl:if>
                    <div class='comment-content'>
                        <xsl:value-of select="./comment/comment" disable-output-escaping="yes" />
					</div>                    
                    <xsl:if test="string-length(./signature) &gt; 0">
                        <div class='user-signature clearfix'>
                            <xsl:value-of select="./signature" disable-output-escaping="yes"/>
                        </div> 
                    </xsl:if>
                </div>
            </div><!-- .comment-text -->
    	</div>
    </xsl:template>
</xsl:stylesheet>