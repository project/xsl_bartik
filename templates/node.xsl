<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
    
    <xsl:template match="/root">
    	<xsl:variable name='nid' select="./nid"/>
        <xsl:if test="//page != 1">
            <h2 class="title">
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select='php:function("url", concat(string("node/"), ./nid))'/>
                    </xsl:attribute>
                    <xsl:value-of select="./title"/>
                </a>
            </h2>
        </xsl:if>
        <xsl:if test="//unpublished = 1">
        	<div class="unpublished">
            	<xsl:value-of select="php:function('t', 'Unpublished')"/>
            	
            </div>
        </xsl:if>
        <xsl:if test="//display_submitted &gt; 0">
            <div class='submitted'>            	
                Submitted by <xsl:value-of select="php:function('l', string(./user/name), concat('user/', string(./user/uid)))" disable-output-escaping="yes"/> on 
                <xsl:value-of select="php:function('format_date', string(./created))"/>
            </div>
        </xsl:if>
        <div class='content'>
        	<xsl:value-of disable-output-escaping="yes" select="content"/>
        </div>
        <xsl:if test="string-length(//terms) &gt; 0">
        	<div class='terms'>
        		<xsl:value-of select="//terms" disable-output-escaping="yes"/>
        	</div>
        </xsl:if>
        
        <xsl:if test="string-length(./links) &gt; 0">
        	<div class="link-wrapper">
            	<xsl:value-of select="./links" disable-output-escaping="yes"/>
            </div>
        </xsl:if>
        
    </xsl:template>    
</xsl:stylesheet>