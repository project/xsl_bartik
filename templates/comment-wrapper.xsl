<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
    <xsl:output method="html"/>
 	<xsl:template match="/">
    	<div id="comments">
        	<xsl:attribute name="class">
            	<xsl:text>comment-wrapper </xsl:text>
                <xsl:for-each select="//classes_array/item">
                	<xsl:value-of select="."/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
            </xsl:attribute>
            <xsl:if test="//node/type != 'forum'">
                <h3 class="title">
                    <xsl:value-of select="php:function('t', 'Comments')"/>
                </h3>
            </xsl:if>
            <xsl:value-of select='//content' disable-output-escaping="yes"/>
        	
            <xsl:if test="string-length(//comment_form) &gt; 0">
            	<h2 class='comment-form title'>
                	<xsl:value-of select="php:function('t', 'Add new comment')" disable-output-escaping="yes"/>
                </h2>
                <xsl:value-of select="//comment_form" disable-output-escaping="yes"/>
            </xsl:if>        
        </div>        
    </xsl:template> 	
</xsl:stylesheet>