<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
	<xsl:output method="html"/>
    <xsl:template name='page' match="/">
        <xsl:param name='vars' select='./root'/>
        <html>
            <head>
                <title><xsl:value-of select='$vars/head_title'/></title>
                <xsl:value-of disable-output-escaping="yes" select='$vars/head'/>
                <xsl:value-of disable-output-escaping="yes" select='$vars/styles'/>
                <xsl:value-of disable-output-escaping="yes" select='$vars/scripts'/>
            </head>
            <body>
            	<xsl:attribute name='class'>
                	<xsl:for-each select="//classes_array/item">
                        <xsl:value-of select="."/>
                        <xsl:text> </xsl:text>
                	</xsl:for-each> 
                </xsl:attribute>
                <div id="page-wrapper">
                	<div id="page">
    					<div id="header">
                        	<xsl:if test="string-length(//secondary_links_menu) &gt; 0">                            
                                <xsl:attribute name="class">
                                    <xsl:text>with-secondary-menu</xsl:text>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:if test="string-length(//secondary_links_menu) = 0">                            
                                <xsl:attribute name="class">
                                    <xsl:text>without-secondary-menu</xsl:text>
                                </xsl:attribute>
                            </xsl:if>
                        	<div class="section clearfix">                                
                                <a id="logo">
                                    <xsl:attribute name='href'>
                                        <xsl:value-of select='//front_page'/>
                                    </xsl:attribute>
                                    <xsl:attribute name='title'>
                                        <xsl:value-of select='//site_title'/>
                                    </xsl:attribute>
                                    <img>
                                        <xsl:attribute name='src'>
                                            <xsl:value-of select="$vars/logo"/>
                                        </xsl:attribute>
                                        <xsl:attribute name='title'>
                                            <xsl:value-of select='$vars/site_title'/>
                                        </xsl:attribute>
                                    </img>                                                                   
                                </a>
                                <xsl:if test="//site_name | //site_slogan">
                                    <div id="name-and-slogan">
                                        <xsl:if test="//hide_site_name">
                                            <xsl:attribute name="class">
                                                <xsl:text>element-invisible</xsl:text>
                                            </xsl:attribute>                                    
                                        </xsl:if>
                                        <xsl:if test="./title">
                                            <div id='site-name'>
                                              <strong>
                                                <a>
                                                    <xsl:attribute name="href">
                                                        <xsl:value-of select="//front_page"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="title">
                                                        <xsl:value-of select="php:function('t', 'Home')"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="rel">
                                                        <xsl:text>home</xsl:text>
                                                    </xsl:attribute> 
                                                    <span>
                                                        <xsl:value-of select="//site_name"/>
                                                    </span>                                          
                                                </a>                                          
                                              </strong>                                        
                                            </div>                                    
                                        </xsl:if>
                                        <xsl:if test="not(./title)">
                                            <h1 id='site-name'>
                                              <strong>
                                                <a>
                                                    <xsl:attribute name="href">
                                                        <xsl:value-of select="//front_page"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="title">
                                                        <xsl:value-of select="php:function('t', 'Home')"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="rel">
                                                        <xsl:text>home</xsl:text>
                                                    </xsl:attribute> 
                                                    <span>
                                                        <xsl:value-of select="//site_name"/>
                                                    </span>                                          
                                                </a>                                          
                                              </strong>                                        
                                            </h1>                                    
                                        </xsl:if>
                                        <xsl:if test="//site_slogan">
                                            <div id='site-slogan'>
                                                <xsl:value-of select="//site_slogan" disable-output-escaping="yes"/>
                                            </div>                                    
                                        </xsl:if>
                                    </div><!-- #site-name-slogan -->
                                </xsl:if>
                                
                                <xsl:value-of disable-output-escaping="yes" select='$vars/header'/>
                                
                                <xsl:if test="string-length(//primary_links_menu) &gt; 0">
                                	<div id="main-menu" class="navigation">                                    	
                                    	<xsl:value-of select="//primary_links_menu" disable-output-escaping="yes"/>
                                	</div>
                                </xsl:if>
                                <xsl:if test="string-length(//secondary_links_menu) &gt; 0">
                                	<div id="secondary-menu" class="navigation">
                                 		<xsl:value-of select="//secondary_links_menu" disable-output-escaping="yes"/>
                                    </div>
                                </xsl:if>
                           </div>
                        </div> <!-- #header -->
                        <xsl:if test="string-length(//messages) &gt; 0">
                            <div id="messages">
                                <div class='section clearfix'>
                                    <xsl:value-of disable-output-escaping="yes" select='$vars/messages'/>
                                </div>
                            </div>
                        </xsl:if>
                        <xsl:if test="string-length(//featured) &gt; 0">
                        	<div id="featured">
                        		<xsl:value-of select="//featured" disable-output-escaping="yes"/>
                        	</div>
                        </xsl:if>
                        <div id="main-wrapper" class="clearfix">
                        	<div id="main">
								<xsl:attribute name="class">
                                	<xsl:text>clearfix </xsl:text>
                                </xsl:attribute>
                                
                                <div id="breadcrumb">
                                	<xsl:value-of select="//breadcrumb" disable-output-escaping="yes"/>
                                </div>
                                
                                <div id='sidebar-first' class='column sidebar'>
                                    <xsl:value-of disable-output-escaping="yes" select='$vars/sidebar_first'/>
                                </div>
                                
      							<div id="content" class="column">
                                	<div class="section">
                                    	<xsl:if test="string-length(//highlighted) &gt; 0">
                                            <div id="highlighted">
                                                <xsl:value-of select='//highlight' disable-output-escaping="yes"/>
                                            </div>
                                        </xsl:if>
                                                                     
                                        <xsl:if test="string-length(//title) &gt; 0">
                                        	<h1 class="title" id="page-title">
                                            	<xsl:value-of select="//title"/>
                                            </h1>
                                        </xsl:if>
                                        
                                        <xsl:if test="string-length(//tabs) &gt; 0">
                                        	<div class='tabs'>
                                        		<xsl:value-of select="//tabs" disable-output-escaping="yes"/>
                                            </div>
                                        </xsl:if>
                                        
                                        <xsl:value-of select="//help" disable-output-escaping="yes"/>
                                        
                                        <xsl:if test="//action_links">
                                        	<xsl:value-of select="//action_links" disable-output-escaping="yes"/>
                                        </xsl:if>
                                        
                                        <div id='content-area'>
                                            <xsl:value-of disable-output-escaping="yes" select='$vars/content'/>
                                        </div>
                                        
                                        <xsl:value-of select="//feed_icons" disable-output-escaping="yes"/>
                                   </div>
                               </div><!-- #content -->        
                                                           
                               <xsl:if test="string-length(//sidebar_second) &gt; 0"> 
                                    <div id='sidebar-second'>
                                        <xsl:value-of disable-output-escaping="yes" select='//sidebar_second'/>
                                    </div>
                               </xsl:if>
                                                                                 
                            </div>
                        </div><!-- #main-wrapper -->
                        <xsl:if test="string-length(//triptych_first | //triptych_middle | //triptych_last) &gt; 0">
                        	<div id="triptych-wrapper">
                            	<div id="triptych" class="clearfix">
                            		<xsl:for-each select="//triptych_first | //triptych_middle | //triptych_last">
                                    	<xsl:value-of select="." disable-output-escaping="yes"/>
                                    </xsl:for-each>
                                </div>
                            </div>                        
                        </xsl:if>
                        
                        <div id="footer-wrapper">
                        	<div class="section">
								<xsl:if test="string-length(//footer_firstcolumn | //footer_secondcolumn | //footer_thirdcolumn | //footer_fourthcolumn) &gt; 0">
                                	<div id="footer_columns" class="clearfix">
                                        <xsl:for-each select="//footer_firstcolumn | //footer_secondcolumn | //footer_thirdcolumn | //footer_fourthcolumn">
                                            <xsl:value-of select="." disable-output-escaping="yes"/>
                                        </xsl:for-each>
                                    </div>
                                </xsl:if> 
                                <xsl:if test="string-length(//footer) &gt; 0">
                                    <div id="footer">
                                        <div class="section">
                                            <xsl:if test="string-length(//footer_message) &gt; 0">
                                                <div id='footer-message'>
                                                    <xsl:value-of disable-output-escaping="yes" select="//footer_message"/>
                                                </div>
                                            </xsl:if>
                                            <xsl:value-of select="//footer" disable-output-escaping="yes"/>
                                        </div>
                                    </div> <!-- #footer -->
                                </xsl:if>               
  							</div>
  						</div>
                    </div>
            	</div>
                <xsl:value-of select="//page_closure" disable-output-escaping="yes"/>
                <xsl:value-of select="//closure" disable-output-escaping="yes"/>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>